#!/bin/bash

# This script will:
#	1) add a yum repo for puppet
#	2) install git
#	3) install puppet
#	4) launch puppet provisioning




# need a check to make sure we aren't on suse or ubuntu
function addYumBased () {

	echo "INFO: Installing base required packages..."
	cp yumrepos/*.repo /etc/yum.repos.d
	yum install puppet -y --enablerepo=epel,puppet
	yum install git -y

}

# Requires puppet with facter to be installed
function getPuppetOS () {

	echo "Choose from one of the following OS flavors for puppet apply."
	ls puppet/manifests/*.pp
	read PUPPETPP

}


function launchPuppetProvision () {

	echo "INFO: ...Launching Puppet Provisioning..."
	puppet apply -v --modulepath puppet/manifests/${PUPPETPP}

}

function bambooElasticSetup () {

	echo "Enter in the URL to your Bamboo server..."
	read BAMSERVER
	echo "You entered in $BAMSERVER ..."

}



addYumBased
getPuppetOS
launchPuppetProvision
bambooElasticSetup
