file { '/opt':
  ensure => directory,
}

include subversion
include git
include mercurial
include xorg-x11
