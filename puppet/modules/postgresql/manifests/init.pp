class postgresql {
  $package = $operatingsystem ? {
    Fedora  => 'mysql-server',
    RedHat  => 'mysql-server',
    default => 'mysql-server',
  }

  package { $package:
    ensure => latest,
  }
}