class mysql {
  $package = $operatingsystem ? {
    Fedora  => 'postgresql-server',
    RedHat  => 'postgresql-server',
    default => 'postgresql-server',
  }

  package { $package:
    ensure => latest,
  }  
}