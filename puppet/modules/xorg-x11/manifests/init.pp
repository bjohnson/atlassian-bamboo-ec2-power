class xorg-x11 {
  # Puppet doesn't support package groups yet
  $packages = $operatingsystem ? {
    Fedora  => [
      'xorg-x11-server-Xorg',
      'tigervnc-server',
      'xorg-x11-server-Xvfb',
    ],
    RedHat  => [
      'xorg-x11-server-Xorg',
      'tigervnc-server',
      'xorg-x11-server-Xvfb',
    ],
    default => [
      'xorg-x11-server-Xorg',
      'tigervnc-server',
      'xorg-x11-server-Xvfb',
    ],
  }

  package { $packages:
    ensure => latest,
  }
}
