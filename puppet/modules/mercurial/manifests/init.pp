class mercurial {
  $package = $operatingsystem ? {
    Fedora  => 'mercurial',
    RedHat  => 'mercurial',
    default => 'mercurial',
  }

  package { $package:
    ensure => latest,
  }
}
