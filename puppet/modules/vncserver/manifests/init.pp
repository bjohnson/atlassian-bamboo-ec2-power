class vncserver {
  $package = $operatingsystem ? {
    Fedora  => 'tigervnc',
    RedHat  => 'tigervnc',
    default => 'tigervnc',
  }

  package { $package:
    ensure => latest,
  }  
}
