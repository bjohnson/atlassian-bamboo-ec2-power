class git {
  $package = $operatingsystem ? {
    Fedora  => 'git',
    RedHat  => 'git',
    default => 'git',
  }

  package { $package:
    ensure => latest,
  }
}
