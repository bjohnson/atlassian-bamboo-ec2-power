class java {
  
  $base = '/opt'

  require File[$base]
  
  puppet-tasks::java-install('x86_64') { [
    'jdk1.5.0_22',
    'jdk1.6.0_31',
    'jdk1.7.0_03',
  ]: }
}
