define puppet-tasks::java-install($arch) {
  puppet-tasks::wget { "$name.$arch.tar.gz":
    target_dir => $java::base,
    creates    => "$java::base/$name.$arch.tar.gz"
  }
  puppet-tasks::untargz { "$java::base/$name.$arch.tar.gz":
    target_dir => $java::base,
    creates    => "$java::base/$name",
    subscribe  => Puppet-utils::Wget["$name.$arch.tar.gz"]
  }
}
