class subversion {
  $package = $operatingsystem ? {
    Fedora  => 'subversion',
    RedHat  => 'subversion',
    default => 'subversion',
  }

  package { $package:
    ensure => latest,
  }
}
